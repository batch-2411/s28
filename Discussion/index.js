// CRUD OPERATIONS

/*
- CRUD operations are the heart of any backend application
_ CRUD stands for Create, Read, Update and Delete
- MongoDB deals with objects as its structure for documents, we can easily create them by providing objexts into our methods
*/

// CREATE: Inserting documents

// INSERT ONE
/*
Syntax:
	db.collectionName.insertOne({})

Inserting/Assigning values in JS Objects:
	object.object.method({object})
*/
db.users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "8923742",
		email: "janedoe@gmail.com"
	},
	courses: ["CSS", "Javascript", "Python"],
	department: "none"
})

// INSERT MANY
/*
Syntax:
	db.collectionName.insertMany([
		{objectA},
		{objectB}
	])
*/
db.users.insertMany([
{	
	firstName: "Stephen",
	lastName: "Hawking",
	age: 76,
	contact: {
		phone: "1827242",
		email: "stephenhawking@gmail.com"
	},
	courses: ["Python", "React", "PHP"],
	department: "none"
},
{	
	firstName: "Neil",
	lastName: "Armstrong",
	age: 82,
	contact: {
		phone: "5362742",
		email: "neilarmstrong@gmail.com"
	},
	courses: ["React", "Laravel", "SASS"],
	department: "none"
}
])


// READ: FINDING DOCUMENTS

// FIND ALL
/*
Syntax:
	db.collectionName.find()
*/
db.users.find();

// Finding users with single arguments
/*
Syntax:
	db.collectionName.finc({field:value})
*/

// Look for Stephen Hawking using firstname
db.users.find({firstName:"Stephen"});

// Finding users with multiple arguments
// With no matches
db.users.find({firstName: "Stephen", age: 20}); // 0 records - no match

// With matching one
db.users.find({firstName: "Stephen", age: 76, lastName: "Hawking"});

// UPDATE: UPDATING DOCUMENTS
// Repeat Hane to be updated

db.users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "8923742",
		email: "janedoe@gmail.com"
	},
	courses: ["CSS", "Javascript", "Python"],
	department: "none"
})

// UPDATE ONE
/*
Syntax:
	db.collectionName.updateOne({criteria} , {$set: (field: value)})
*/

db.users.updateOne(
	{firstName: "Jane"},
	{
		$set: {
			firstName: "Jane",
			lastName: "Gates",
			age: 65,
			contact: {
				phone: "8923742",
				email: "janedoe@gmail.com"
			},
			courses: ["AWS", "Google Cloud", "Azure"],
			department: "infrastructure",
			status: "active"
		}
	}
	);
db.users.find({firstName: "Jane"});

// UPDATE MANY
db.users.updateMany(
	{department: "none"},
	{
		$set: {
			department: "HR"
		}
	}
);
db.users.find().pretty();

// REPLACE ONE
/*
- Can be used if replacing the whole document is necessary
- Syntax:
	db.collectionName.replaceOne( {criteria},  {field: value})
*/
db.users.replaceOne(
	{ lastName: "Gates"},
	{
			firstName: "Bill",
			lastName: "Clinton"
	}
)

db.users.find({firstName: "Bill"});

// DELETE: DELETING DOCUMENTS
db.users.insert({
	firstName: "Test",
	lastName: "Test",
	age: 0,
	contact: {
		phone: "00000",
		email: "test@gmail.com"
	},
	courses: [],
	department: "none"
});
db.users.find({firstName: "Test"});

// Deleting a single document
/*
Syntax:
	db.collectionName.deleteOne({criteria})
*/
db.users.deleteOne({firstName: "Test"});
db.users.find({firstName: "Test"});

db.users.deleteOne({age: 76});
db.users.find({age: 76});

// Delete Many
db.users.deleteMany({courses: []});
db.users.find();

// Delete all
// db.collectionName.deleteMany()
/*
- Be careful when using the "deleteMany" method. If no search criteria is provided, it will delete all documents
- DO NOT USE: db.collectionName.deleteMany()
- Syntax:
	db.collectionName.deleteMany({criteria})
*/

// ADVANED QUERIES

// Query an embedded document
	db.users.find({
	contact: {
		phone: "8923742",
		email: "janedoe@gmail.com"
	},
})

// Find the document with the email "janedoe@gmail.com"
// Querying on nested fields
db.users.find({"contact.email": "janedoe@gmail.com"});

// Querying an Array with regard to order of the array
db.users.find({courses: ["React", "Laravel", "SASS"]});

// Querieng an element without regard to order of the array
db.users.find({courses: {$all: ["React", "Laravel", "SASS"]}});

// Make an array to query
db.users.insert({
	namearr: [
	{
		namea: "Jane",
	},
	{
		nameb: "Tamad"
	}
	]
})

// find
db.users.find({
	namearr: {
		nameb: "Tamad"
	}
})